# References

## CoC installation and setup

- [CoC on github][1]
- [Configure coc.nvim for C/C++ Development][2]
- [vim configs for golang, python, and rust in 2020 with coc.nvim][3]
- [Vim switch YCM to LSP, coc.nvim And Vimgo][4]

## `asdf` installation and setup

- [asdf official docs][5]
- [Switching from pyenv, rbenv, goenv and nvm to asdf][6]
- [Setting up your computer for the best Python application development experience][7]
- [asdf][8]

## Terminal environment setup

- [How to download the latest release from github][9]
- [How to install stable version of neovim on Ubuntu 18.04][10]
- [TLDR: Python dependencies on Ubuntu][11]


[1]: https://github.com/neoclide/coc.nvim
[2]: https://ianding.io/2019/07/29/configure-coc-nvim-for-c-c++-development/
[3]: https://todebug.com/vim-configs-new/
[4]: https://huifeng.me/posts/vimuselspandvimgormycm/
[5]: https://asdf-vm.com/#/core-manage-asdf
[6]: https://jinyuz.dev/posts/tips-and-tricks/Switching-from-pyenv,-rbenv,-goenv-and-nvm-to-asdf
[7]: https://abbasegbeyemi.me/blog/python-asdf-virtualenv
[8]: https://www.mikeabney.dev/notes/laptop/asdf/
[9]: https://starkandwayne.com/blog/how-to-download-the-latest-release-from-github/
[10]: https://vi.stackexchange.com/questions/25192/how-to-install-stable-version-of-neovim-on-ubuntu-18-04
[11]: https://humberto.io/blog/tldr-python-dev-dependencies-on-ubuntu/
