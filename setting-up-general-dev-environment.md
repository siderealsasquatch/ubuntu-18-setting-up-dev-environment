# Setting up a dev environment

This guide briefly covers how to set up a general dev environment on Ubuntu Linux using
`asdf`.

## Preliminary steps

Before we start installing anything we'll start by installing all of the necessary
libraries:

```
# Most of these packages are for Python development. Will add more as I begin using other
# languages.
sudo apt update
sudo apt install dpkg-dev build-essential python-dev python3-dev libncursesw5-dev \
  libsqlite3-dev libreadline-dev libbz2-dev libffi-dev libssl-dev libgdbm-dev zlib1g-dev \
  libjpeg-dev libtiff-dev libpq-dev libxml2-dev libxslt1-dev libsdl2-dev \
  libgstreamer-plugins-base1.0-dev libnotify-dev freeglut3-dev libsm-dev libgtk-3-dev \
  libwebkitgtk-3.0-dev libxtst-dev
```

## `asdf` installation

Make sure that you have git and curl installed on your system. You can check using either
the `which`/`whence` command (depending on your shell) or using `dpkg` (if you know the
package names). In the official docs you can either clone just the latest release or clone
the entire repo and checkout the latest release. We'll go with the latter:

```
git clone https://github.com/asdf-vm/asdf.git ~/.asdf
cd ~/.asdf
git checkout "$(git describe --abbrev=0 --tags)"
```

After the commands have finished running make sure to include the `asdf` plugin in the
array of `oh-my-zsh` plugins:

```
plugins=(... asdf)
```

Restart your shell using `exec $SHELL`. You should now have `asdf` installed. Verify this
by running `asdf -h` or `which asdf`.

## Setting up `asdf`

Before you can install different language versions you'll need to first install the
plugin for the language that you want to develop with. Here, we're going to be working
mostly with Python but I've also installed node since I need it for the CoC neovim plugin
and I do plan on learning webdev in the future. Before we can install the plugins we need
to do a bit of extra work for node. We first need to import the gpg keys using:

```
bash -c '${ASDF_DATA_DIR:=$HOME/.asdf}/plugins/nodejs/bin/import-release-team-keyring'
```

Then we install the plugins with:

```
asdf plugin add python
asdf plugin add nodejs
```

We can now install our desired versions of Python and node. Here we'll install the latest
version of both:

```
asdf install python latest
asdf install node latest
```

And then we'll set the latest version as the global version with:

```
# At the time of writing the latest version of Python was 3.9.1 and the latest version of
# node was 15.7.0
adsf global python 3.9.1
asdf global node 15.7.0
```

## Setting up `virtualenv` and `virtualenvwrapper`

Prior to `asdf` I used `pyenv` to manage python versions and virtual environments and
before `pyenv` I used `virtualenvwrapper`. Since I'm moving away from `pyenv` I'm going
back to managing virtual environments with `virtualenvwrapper`. First, we install the
necessary packages with:

```
pip install virtualenv{,wrapper}
```

Afterwards, create a directory to store all of the `virtualenvwrapper` artifacts and add
the path to this directory to your `.zshrc` file:

```
# I named my virtualenv directory '.py-venvs' but you can use whatever name you like
mkdir .py-venvs

# Add the following to your .zshrc files
export WORKON_HOME=~/.py-venvs
. $(asdf where python)/bin/virtualenvwrapper.sh
```

Restart your shell with `exec $SHELL` and then run `asdf reshim` so that `asdf` picks up
on the newly installed tools. Check that everything works by creating a test environment:

```
mkvirtualenv test-env
```

You should see messages indicating that a new virtual environment has been created before
being dropped into it. The `spaceship` prompt should pick up on the new virtual
environment.
