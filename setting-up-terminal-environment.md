# Setting up a terminal environment

You should have already followed the guide that you wrote for doing the preliminary dev
setup for Arch Linux so stuff like creating ssh keys for your GitHub and GitLab accounts
isn't covered here. What will be covered is how to install the programs that we'll use for
our daily dev work.

## Install the latest version of NeoVim

Ubuntu 18.04 ships with an older version of NeoVim in its repos. Some of the plugins that
we'll be using require a later version of NeoVim in order to work properly. Thankfully
there's a PPA that provides the latest stable version of NeoVim. To install NeoVim with
this PPA, run the following commands:

```
sudo add-apt-repository ppa:neovim-ppa/stable
sudo apt-get update
sudo apt-get install neovim
```

## Installing the necessary terminal apps

Here we'll outline the steps necessary for installing the following apps:

- `jq`
- `fd`
- `ripgrep`
- `bat`

To install `jq`, simply run:

```
sudo apt install jq
```

The other apps are unfortunately not part of the repos. We'll have to install the release
version from the GitHub repos. Since we're using WSL we can't simply drag and drop the
file into the Downloads directory. We'll have to actually download the release files from
the GitHub page directly. We can do this with the following bit of code:

```
# This will download the .deb file for ripgrep into the current directory
curl -fLO $(curl -s https://api.github.com/repos/BurntSushi/ripgrep/releases/latest | jq \
  -r ".assets[] | select(.name | test(\"amd64\")) | .browser_download_url")
```

Running the oneliner in the command substitution simply returns the download URL. For
`ripgrep` there is only one URL. However, for `fd` and `bat` there are two. Since I
haven't quite figured out how to properly retrieve just the URL that we want we'll have to
run the oneliner in the command substitution with the url to the release page of the
respective app, copy the URL that we want, and paste it into the outer `curl` statement.
Once you've downloaded all the .deb files you can install them using
`sudo dpkg -i <deb_file>`.
